-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 03:54 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mofatama`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_mofatama`
--

CREATE TABLE IF NOT EXISTS `about_mofatama` (
  `id_about` int(11) NOT NULL,
  `kategori_about` varchar(35) NOT NULL,
  `title_about` varchar(40) NOT NULL,
  `konten_about` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `business_connection`
--

CREATE TABLE IF NOT EXISTS `business_connection` (
  `id_business` int(5) NOT NULL,
  `nama_business` varchar(40) NOT NULL,
  `keterangan_business` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `header_slider`
--

CREATE TABLE IF NOT EXISTS `header_slider` (
  `id_slide` varchar(10) NOT NULL,
  `img_src` varchar(30) NOT NULL,
  `urutan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `header_slider_direction`
--

CREATE TABLE IF NOT EXISTS `header_slider_direction` (
  `id_direction` varchar(20) NOT NULL,
  `id_slide` varchar(10) NOT NULL,
  `layer1_title` varchar(30) NOT NULL,
  `layer2_title` varchar(30) NOT NULL,
  `layer3_btn_title_a` varchar(10) NOT NULL,
  `layer3_btn_title_b` varchar(10) NOT NULL,
  `layer3_btn_url_a` varchar(20) NOT NULL,
  `layer3_btn_url_b` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor_financial`
--

CREATE TABLE IF NOT EXISTS `investor_financial` (
  `id_investor` int(5) NOT NULL,
  `kategori_investor` varchar(25) DEFAULT NULL,
  `isi_investor` text,
  `file_url` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newsnevents`
--

CREATE TABLE IF NOT EXISTS `newsnevents` (
  `id_news` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `isi` text,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_content`
--

CREATE TABLE IF NOT EXISTS `site_content` (
  `id_content` int(3) NOT NULL,
  `content_about_img` varchar(40) DEFAULT NULL,
  `content_about_text` text,
  `content_visi` text,
  `content_misi` text,
  `content_video_url` varchar(40) DEFAULT NULL,
  `content_video_ratio` int(5) DEFAULT NULL,
  `content_video_title` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_content`
--

INSERT INTO `site_content` (`id_content`, `content_about_img`, `content_about_text`, `content_visi`, `content_misi`, `content_video_url`, `content_video_ratio`, `content_video_title`) VALUES
(1, '', 'Usaha batubara dimulai pada tahun 2008. Dengan memulai dari bidang trading batubara, baik lokal maupun eksport. Melihat pertumbuhan usaha yang meningkat dari perkembangan batubara yang sangat bagus serta prospek pemasaran yang sangat besar, perusahaan mulai melakukan expansi usaha, yaitu pada tahun 2010 perusahaan mengakuisisi tambang batubara yang berlokasi di Sungai Danau, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan seluas 41,5 Ha, sehingga sumber batubara selalu terjaga dengan baik untuk memenuhi permintaan pasar. Kemudian berturut-turut di tahun 2011 mengakuisisi koneksi tambang di Barito Utara Provinsi Kalimantan Tengah seluas 193,5 Ha, 21.094 Ha, 3211 Ha. dan di Kutai Timur, Provinsi Kalimantan TImur seluas 10.220 Ha. Semua itu dilakukan untuk menambah jumlah cadangan batubara untuk beberapa tahun kedepan, sehingga perusahaan bisa memenuhi permintaan pasar. Saat ini produksi berjalan 250.000 ton perbulan (3 juta pertahun) dan akan terus di tingkatkan kapasitas penambangannya sesuai permintaan.', 'Menjadi perusahaan energi yang handal dan mampu memenuhi komitmen kepada pihak stakeholder.', 'Peningkatan mutu sumber daya manusia yang secara terus menerus, sehingga dapat bekerja secara profesional dan mempunyai kemampuan yang memadai dalam proses pertambangan.', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_meta`
--

CREATE TABLE IF NOT EXISTS `site_meta` (
  `title_site` varchar(20) DEFAULT NULL,
  `description_site` varchar(200) DEFAULT NULL,
  `keywords_site` varchar(30) DEFAULT NULL,
  `author_site` varchar(50) DEFAULT NULL,
  `theme_color_site` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_meta`
--

INSERT INTO `site_meta` (`title_site`, `description_site`, `keywords_site`, `author_site`, `theme_color_site`) VALUES
('Mofatama Energi', '', 'Pertambangan, Mofatama, Energi', '', '#3C7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_mofatama`
--
ALTER TABLE `about_mofatama`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `business_connection`
--
ALTER TABLE `business_connection`
  ADD PRIMARY KEY (`id_business`);

--
-- Indexes for table `header_slider`
--
ALTER TABLE `header_slider`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `header_slider_direction`
--
ALTER TABLE `header_slider_direction`
  ADD KEY `id_slide` (`id_slide`);

--
-- Indexes for table `investor_financial`
--
ALTER TABLE `investor_financial`
  ADD PRIMARY KEY (`id_investor`);

--
-- Indexes for table `newsnevents`
--
ALTER TABLE `newsnevents`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `site_content`
--
ALTER TABLE `site_content`
  ADD PRIMARY KEY (`id_content`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_mofatama`
--
ALTER TABLE `about_mofatama`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `business_connection`
--
ALTER TABLE `business_connection`
  MODIFY `id_business` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `investor_financial`
--
ALTER TABLE `investor_financial`
  MODIFY `id_investor` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsnevents`
--
ALTER TABLE `newsnevents`
  MODIFY `id_news` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_content`
--
ALTER TABLE `site_content`
  MODIFY `id_content` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `header_slider_direction`
--
ALTER TABLE `header_slider_direction`
ADD CONSTRAINT `header_slider_direction_ibfk_1` FOREIGN KEY (`id_slide`) REFERENCES `header_slider` (`id_slide`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
