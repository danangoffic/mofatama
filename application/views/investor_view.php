<!DOCTYPE html>
<html lang="en">
<?php 
$data['active'] = $active;
$this->load->view('templates/head', $data); ?>

<body>

  <?php $this->load->view('templates/header'); ?>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" style="max-height: 100px;">
    
  </section><!-- #intro -->

  <main id="main">

    <section id="portfolio"  class="section-bg" >
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">BISNIS KAMI</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter=".filter-fi" class="filter-active">Financial Information</li>
              <li data-filter=".filter-ar">Annual Report</li>
              <li data-filter=".filter-si">Shareholders Information</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">
          <div class="col-lg-12 col-md-12 portfolio-item filter-fi wow fadeInUp" style="height: 980px !important;">
            <div class="portfolio-wrap" style="height: 100% !important;">         
              <div class="portfolio-info" style="height: 100% !important;">
                <!-- <embed src="path/filename.pdf" width="500" height="375"> -->
                <embed src="<?=base_url('content/b.pdf');?>#toolbar=1#toolbar=1" type="application/pdf" width="100%" height="600px" style="width: 100% !important;height: 100% !important;border: none;" />
                
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 portfolio-item filter-ar wow fadeInUp" style="height: 980px !important;">
            <div class="portfolio-wrap" style="height: 100% !important;">         
              <div class="portfolio-info" style="height: 100% !important;">
                <embed src="<?=base_url('content/c.pdf');?>#toolbar=1#toolbar=1" style="width: 100% !important;height: 100% !important;border: none;">
                </embed>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 portfolio-item filter-si wow fadeInUp" style="height: 980px !important;">
            <div class="portfolio-wrap" style="height: 100% !important;">         
              <div class="portfolio-info" style="height: 100% !important;">
                <embed src="<?=base_url('content/a.pdf');?>#toolbar=1#toolbar=1" style="width: 100% !important;height: 100% !important;border: none;">
                </embed>
              </div>
            </div>
          </div>
        </div>

        <!-- <?php //if(isset($content)): ?>

        <div class="row portfolio-container">
          <?php
          //foreach ($content as $row) {
            ?>
            <div class="col-lg-12 col-md-12 portfolio-item wow fadeInUp">
              <div class="portfolio-wrap">         
                <div class="portfolio-info">
                  <h4><?=$row->nama_business;?></h4>
                  <p><?=$row->keterangan_business;?></p>
                </div>
              </div>
            </div>
            <?php
          //}
          ?>
        </div>
        <?php //endif; ?> -->

      </div>
    </section><!-- #portfolio -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>Mofatama Energi</h3>
            <p>Usaha batubara dimulai pada tahun 2008. Dengan memulai dari bidang trading batubara, baik lokal maupun eksport. Saat ini produksi berjalan 250.000 ton perbulan (3 juta pertahun) dan akan terus di tingkatkan kapasitas penambangannya sesuai permintaan.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url();?>">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('about');?>">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('business');?>">Our Businesses</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('blog');?>">News & Events</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('investors');?>">Investor Relations</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              <address><?=$this->about->info()->row()->address;?></address>
              <strong>Phone:</strong> <a href="tel:<?=$this->about->info()->row()->phone;?>"><?=$this->about->info()->row()->phone;?></a><br>
              <strong>Email:</strong> <a href="<?=$this->about->info()->row()->email;?>"><?=$this->about->info()->row()->email;?></a><br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <!-- <p></p> -->
            <hr>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Mofatama Energi</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <?php $this->load->view('templates/scriptjs'); ?>

</body>
</html>
