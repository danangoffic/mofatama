<!DOCTYPE html>
<html lang="en">
<?php
$data['active'] = $active;
 $this->load->view('templates/head'); ?>

<body>

  <?php $this->load->view('templates/header'); ?>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" style="max-height: 100px;">
    
  </section><!-- #intro -->

  <main id="main">
    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container-fluid">
        <div class="section-header wow fadeInUp">
          <h3>Story of "Mofatama Energi"</h3>
          <!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p> -->
        </div>

        <div class="row">

          <div class="col-lg-12 col-md-12 wow fadeInUp">
            <div class="member">
              <img src="<?=base_url('images/mof_5.jpg')?>" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Armada</h4>
                  <span>Armada Mofatama Energi</span>
                  <!-- <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- #team -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <!-- <?php var_dump($dabout); ?> -->
          <h3>About Us</h3>
          <p><?=$this->about->by_kategori('about')->row()->konten_about;?></p>
        </header>

        <div class="row about-cols">
          <div class="col-md-6 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="img/about-mission.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Mission</a></h2>
              <p><?=$this->about->by_kategori('mission')->row()->konten_about;?></p>
            </div>
          </div>
          <div class="col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-vision.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-eye-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Vision</a></h2>
              <p><?=$this->about->by_kategori('vision')->row()->konten_about;?></p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- #about -->
    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Our Clients</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="template-sample/img/clients/client-1.png" alt="">
          <img src="template-sample/img/clients/client-2.png" alt="">
          <img src="template-sample/img/clients/client-3.png" alt="">
          <img src="template-sample/img/clients/client-4.png" alt="">
          <img src="template-sample/img/clients/client-5.png" alt="">
          <img src="template-sample/img/clients/client-6.png" alt="">
          <img src="template-sample/img/clients/client-7.png" alt="">
          <img src="template-sample/img/clients/client-8.png" alt="">
        </div>

      </div>
    </section> --><!-- #clients -->

    <section id="orgstructural" class="wow fadeInUp">
      <div class="container">
        <header class="section-header">
          <h3>Organization Structural</h3>
        </header>

        <div class="row">
          
        </div>
      </div>
    </section>

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Contact Us</h3>
          <hr>
          <p></p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address><?=$this->about->info()->row()->address;?></address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:<?=$this->about->info()->row()->phone;?>"><?=$this->about->info()->row()->phone;?></a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="<?=$this->about->info()->row()->email;?>"><?=$this->about->info()->row()->email;?></a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="<?=base_url('message');?>" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>Mofatama Energi</h3>
            <p>Usaha batubara dimulai pada tahun 2008. Dengan memulai dari bidang trading batubara, baik lokal maupun eksport. Saat ini produksi berjalan 250.000 ton perbulan (3 juta pertahun) dan akan terus di tingkatkan kapasitas penambangannya sesuai permintaan.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url();?>">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('about');?>">About us</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('business');?>">Our Businesses</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('blog');?>">News & Events</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="<?=base_url('investors');?>">Investor Relations</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
              <?=$this->about->info()->row()->address;?>
              <strong>Phone:</strong> <?=$this->about->info()->row()->phone;?><br>
              <strong>Email:</strong> <?=$this->about->info()->row()->email;?><br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p></p>
            <hr>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Mofatama Energi</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <?php $this->load->view('templates/scriptjs'); ?>

</body>
</html>
