<!DOCTYPE html>
<html lang="en">
<?php 
// $data['active'] = $active;
$this->load->view('templates/head', $data); ?>

<body>

  <?php $this->load->view('templates/header'); ?>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" style="max-height: 100px;">
    
  </section><!-- #intro -->

  <main id="main" style="margin-top: 20px; margin-bottom: 20px; min-height: 500px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <?php if(isset($data)): 
          function limit_words($string, $word_limit){
                  $words = explode(" ",$string);
                  return implode(" ",array_splice($words,0,$word_limit));
          }
          foreach ($data as $row) :
          ?>
          <div class="post-preview">
            
            <!-- <div class="portfolio-wrap"> -->
              <a href="<?=base_url('Blog/view/').$row->id_eventnews;?>">
                <h2 class="post-title"><?=ucwords($row->title_eventnews);?></h2>
              </a>
              <h5 class="post-subtitle"><?php echo limit_words($row->content_eventnews,30)."<a href='".base_url('Blog/view/').$row->id_eventnews."'> Selengkapnya ></a>";?></h5>
                <small class="post-meta">
                  <?php
                  // $dates =  date_format(, "M d Y H:i:s");
                  $submitter = $row->username;
                  $timestamp = strtotime($row->date_eventnews);
                  ?>
                  Posted By <a href=""></a><?=$submitter;?> On <?=date('F d, Y', $timestamp);?>
                </small> 
                <hr> 
          </div>
          <?php
        endforeach;
        endif;
        ?>
        </div>
      </div>
    </div>

  </main>

  <?php $this->load->view('templates/footer'); ?>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <?php $this->load->view('templates/scriptjs'); ?>

</body>
</html>
