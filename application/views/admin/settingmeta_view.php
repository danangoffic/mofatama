<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('admin/templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-wrench"></i> Setting Base Site
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <?php 
                        if($datameta!="" || !empty($datameta) || $datameta != 0):
                        ?>
                            <form class="form-horizontal" id="" action="<?=base_url('admin/Base_site/save_meta');?>" method="POST">
                                <div class="form-group">
                                    <label for="title_site" class="label-control col-lg-3">
                                        Title Site:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="title_site" name="title_site" placeholder="Title Site.." type="text" value="<?=$datameta->title_site;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="desc_meta">
                                        Description Site:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="desc_meta" name="desc_meta" placeholder="Description Site..." type="text" value="<?=$datameta->description_site;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="keywords_site">
                                        Keyword Site:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="keywords_site" name="keywords_site" placeholder="Keyword Site.." type="text" value="<?=$datameta->keywords_site;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="author_site">
                                        Author Site:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="author_site" name="author_site" placeholder="Author Site..." type="text" value="<?=$datameta->author_site;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="theme_color_site">
                                        Theme Color:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control jscolor" id="theme_color_site" name="theme_color_site" placeholder="Color Theme for site" value="<?=$datameta->theme_color_site;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="address">
                                        Address:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="address" name="address" type="text" value="<?=$datameta->address;?>" placeholder="Address....">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="phone">
                                        Phone:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="phone" name="phone" type="tel" size="12" maxlength="12" placeholder="Phone Number / Telephone..." value="<?=$datameta->phone;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label-control col-lg-3" for="email">
                                        Email:
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="email" name="email" id="email" placeholder="email..." value="<?=$datameta->email;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php
                    else:
                    ?>
                    <h2>Data Kosong, Silahkan tambahkan kategori 'Tentang' pada tombol <i class="fa fa-fw fa-plus-circle"></i></h2>
                    <?php
                    endif;
                    ?>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('admin/templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>