<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('admin/templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-wrench"></i> Setting Business Page
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    <div class="col-lg-12">
                        <a class="btn btn-primary btn-sm pull-right" href="<?=base_url('admin/Business_page/')?>new_page"><i class="fa fa-fw fa-plus "></i>Add Page</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-condensed table-bordered table-hover table-striped" id="tabel_eventsnews">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No</th>
                                    <th class="text-center">Business Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach ($databusiness as $row) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no;?></td>
                                        <td class="text-center">
                                            <a href="<?=base_url(
                                            'admin/Business_page/edit/').$row->id_business;?>"><?=$row->nama_business;?></a>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?=base_url('admin/Business_page/delete/').$row->id_business;?>">delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('admin/templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>