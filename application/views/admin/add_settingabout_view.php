<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('admin/templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li>
                                <i class="fa fa-book-open"></i> Posts Blog
                            </li>
                            <li class="active">
                                <i class="fa fa-plus-circle"></i> Add Posts
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <form id="form-new-post" action="<?=base_url('admin/About_page/add_about_page');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                            <div class="form-group">
                                <input type="text" class="form-control input-bg" name="title_about" id="title_about" placeholder="Title" autocomplete="off"/>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control input-bg" name="content_news" id="content_news" placeholder="Content.."></textarea>
                            </div>

                            <div class="form-group">
                                <input class="form-control input-sm" type="file" accept="image/*" name="image_news" id="image_news">
                            </div>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->
        <!-- <div class="modal fade" id="add_category" role="dialog">
            <div class="modal-dialog">
            
              
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Uraian</h4>
                </div>
                <div class="modal-body">
                    <form id="form_add_category" class="form-horizontal" role="form" method="POST">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="label-control">Title Category: </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="title_category" id="title_category" class="form-control" placeholder="Category...">
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="button" id="save_category1" class="btn btn-primary">Simpan</button>
                            <button type="button" onclick="this.form.reset();" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                  
                </div>
                <div class="modal-footer">
                    
                </div>
                
              </div>
              
            </div>
        </div>
 -->
    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('admin/templates/script'); ?>
        <script type="text/javascript">
          $(function () {
            CKEDITOR.replace('content_news');
          });
        </script>
        <script type="text/javascript">

            $("#save_category1").click(function(){
                $.post("<?=base_url('admin/Blog_page/save_category')?>", $("#form_add_category").serialize(), function(es){
                    get_category();
                })
            });
        </script>
        <!-- END JS SCRIPT -->  
</body>
</html>