<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('admin/templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-book-open"></i> Posts Blog
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    <div class="col-lg-12">
                        <a class="btn btn-primary btn-sm pull-right" href="<?=base_url('admin/Blog_page/')?>add_post"><i class="fa fa-fw fa-plus "></i>Add Post</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-condensed table-bordered table-hover table-striped" id="tabel_eventsnews">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Submitted By</th>
                                    <th class="text-center">Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $data = $this->AdminModel->get_all_eventnews();
                                foreach ($data as $row) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no;?></td>
                                        <td class="text-center">
                                            <a href="<?=base_url('admin/Blog_page/view_post/').$row->id_eventnews;?>"><?=$row->title_eventnews?></a>
                                        </td>
                                        <td class="text-center"><?=$row->title_category;?></td>
                                        <td class="text-center"><?=$row->username;?></td>
                                        <td class="text-center"><?=$row->date_eventnews;?></td>
                                        <td class="text-center">
                                            <a href="<?=base_url('admin/Blog_page/deletepost/').$row->id_eventnews;?>">Delete Post</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('admin/templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>