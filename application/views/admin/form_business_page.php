<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('admin/templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li>
                                <i class="fa fa-wrench"></i> Setting Business Page
                            </li>
                            <li class="active">
                                <i class="fa fa-plus-circle"></i> Add Page
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <form action="<?=base_url('admin/Business_page/').$cond;?>" method="POST" autocomplete="off">
                            <div class="form-group">
                                <input type="text" class="form-control input-bg" name="nama_business" id="nama_business" placeholder="Nama Business" autocomplete="off" autofocus="" value="<?=$nama_business?>" />
                            </div>
                            <div class="form-group">
                                <textarea class="form-control input-bg" name="keterangan_business" id="keterangan_business" placeholder="Keterangan.."><?=$keterangan_business?></textarea>
                            </div>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->
        
    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('admin/templates/script'); ?>
        <script type="text/javascript">
          $(function () {
            CKEDITOR.replace('content_news');
          });
        </script>
        <script type="text/javascript">

            $("#save_category1").click(function(){
                $.post("<?=base_url('admin/Blog_page/save_category')?>", $("#form_add_category").serialize(), function(es){
                    get_category();
                })
            });
        </script>
        <!-- END JS SCRIPT -->  
</body>
</html>