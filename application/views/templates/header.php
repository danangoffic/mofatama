<!--==========================
    Header
  ============================-->
  <nav id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="<?=base_url();?>" class="scrollto">MofatamaEnergi</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <?php if(!$this->session->lang||$this->session->lang=="id"): ?>
            <li <?php if($active=="home"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>">Home</a></li>
            <li <?php if($active=="about"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>About">Tentang Kami</a></li>
            <li <?php if($active=="business"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>Business">Bisnis Kami</a></li>
            <li <?php if($active=="blog"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>Blog">Berita Dan Acara</a></li>
            <li <?php if($active=="investors"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>InvestorRelation">Hubungan Investor</a></li>
            <li <?php if($active=="contact"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>contact">Kontak</a></li>
            <?php elseif($this->session->lang == 'eng'): ?>
            <li <?php if($active=="home"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>">Home</a></li>
            <li <?php if($active=="about"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>About">About Us</a></li>
            <li <?php if($active=="business"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>Business">Our Business</a></li>
            <li <?php if($active=="blog"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>Blog">News and Events</a></li>
            <li <?php if($active=="investors"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>InvestorRelation">Investor Relation</a></li>
            <li <?php if($active=="contact"){echo "class='menu-active'";} ?>><a href="<?=base_url();?>contact">Contact</a></li>
            <?php endif; ?>
          <li class="menu-has-children">
              <a href="#" >Lang</a>
              <ul class="dropdown-menu">
                <li ><a href="<?=base_url('lang/to/Eng').$_SERVER['QUERY_STRING'];?>">Eng</a></li>
                <li ><a href="<?=base_url('lang/to/Id').$_SERVER['QUERY_STRING'];?>">Id</a></li>
              </ul>
            </li>
          
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>

  </nav><!-- #header -->