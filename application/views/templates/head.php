<head>
  <meta charset="utf-8">
  <title><?=$title_site;?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="<?=$keywords_site;?>" name="keywords">
  <meta content="<?=$description_site;?>" name="description">
  <meta name="keywords" content="<?=$keywords_site;?>">
  <meta name="Theme-color" content="<?=$theme_color_site;?>">
  <meta name="Author" content="<?=$author_site;?>">

  <!-- Favicons -->
  <link href="<?=base_url('template-sample');?>/img/favicon.png" rel="icon">
  <link href="<?=base_url('template-sample');?>/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?=base_url('template-sample');?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?=base_url('library/css/clean_blog.css')?>">

  <!-- Libraries CSS Files -->
  <link href="<?=base_url('template-sample');?>/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?=base_url('template-sample');?>/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?=base_url('template-sample');?>/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?=base_url('template-sample');?>/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url('template-sample');?>/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?=base_url('template-sample');?>/css/style.css" rel="stylesheet">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>