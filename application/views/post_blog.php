<!DOCTYPE html>
<html lang="en">
<?php 
$this->load->view('templates/head', $data); ?>

<body>

  <?php $this->load->view('templates/header'); ?>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" style="max-height: 100px;">
    
  </section><!-- #intro -->

  <main id="main" style="margin-top: 20px; margin-bottom: 20px; min-height: 500px;">
    <article>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 mx-auto">
            <?php
                $submitter = $data->username;
                $timestamp = strtotime($data->date_eventnews);
                ?>
                <h2><?=ucwords($data->title_eventnews);?></h2>
                <p><?="Posted by ".$submitter ." on ". date('F d, Y', $timestamp);?></p>
                <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 mx-auto">
            <?php 
            echo $data->content_eventnews;
          ?>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <button type="button" class="btn btn-primary btn-sm" onclick="return window.location.assign('<?=base_url('Blog');?>')">Back</button>
        </div>
      </div>    
    </article>
  </main>

  <?php $this->load->view('templates/footer'); ?>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <?php $this->load->view('templates/scriptjs'); ?>

</body>
</html>
