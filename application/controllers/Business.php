<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Business extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("AboutModel", 'about');
		$this->load->model('Business_model');
		$this->load->model('site_model');
		$this->site_meta = $this->site_model->site_meta();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		$data = array('title_site'=>$site_meta['title_site']);
		$data['description_site'] = $site_meta['description_site'];
		$data['keywords_site'] = $site_meta['keywords_site'];
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['content'] = $this->Business_model->listofbusiness();
		$data['active'] = 'business';
		$this->load->view('business_view', $data);
	}
}