<?php
/**
 * 
 */
class Contact extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("AboutModel", 'about');
		$this->load->model("Contact_model");
		$this->load->model('site_model');
		$this->site_meta = $this->site_model->site_meta();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		$data = array('title_site'=>$site_meta['title_site']);
		$data['description_site'] = $site_meta['description_site'];
		$data['keywords_site'] = $site_meta['keywords_site'];
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['address'] = $site_meta['address'];
		$data['phone'] = $site_meta['phone'];
		$data['email'] = $site_meta['email'];
		$data['active'] = 'contact';
		$this->load->view('contact_view', $data);
	}

	public function submit_message()
	{
		$fullname = $this->input->post("fullname");
		$email = $this->input->post("email");
		$subject = $this->input->post("subject");
		$message = $this->input->post("message");
		$data = array('fullname'=>$fullname, 'email' => $email, 'subject' => $subject, 'message' => $message);
		if($this->Contact_model->insert_contact($data)):
			echo "OK";
		endif;
	}
}