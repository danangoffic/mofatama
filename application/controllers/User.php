<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class User extends CI_Controller
{
	public $title;
	function __construct()
	{
		parent::__construct();
		$this->title = "Mofatama Energi";	
	}

	public function index()
	{
		$this->title .= " - Home";
		$data = array('title'=>$this->title);
		$this->load->view('home', $data);
	}
}
