<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class About extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("AboutModel", 'about');
		$this->load->model("site_model");
		$this->site_meta = $this->site_model->site_meta();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		// $data['mission'] = $this->about->all();
		$data = array(
				'title_site'=>$site_meta['title_site'], 
				'active'=>'about', 
				'description_site'=>$site_meta['description_site'],
				'keywords_site' => $site_meta['keywords_site'],
				'author_site' => $site_meta['author_site'],
				'theme_color_site' => $site_meta['theme_color_site'],
				'mission' => $this->about->by_kategori('mission')->row(),
				'vision' => $this->about->by_kategori('vision')->row(),
				'dabout' => $this->about->by_kategori('about')->row());
		$this->load->view('about', $data);
	}
}