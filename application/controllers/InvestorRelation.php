<?php
/**
 * 
 */
class InvestorRelation extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("AboutModel", 'about');
		$this->load->model("InvestorRelation_model");
		$this->model_investor = $this->InvestorRelation_model;
		$this->load->model('site_model');
		$this->site_meta = $this->site_model->site_meta();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		$data = array('title_site'=>$site_meta['title_site']);
		$data['description_site'] = $site_meta['description_site'];
		$data['keywords_site'] = $site_meta['keywords_site'];
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['content'] = $this->model_investor->get_all_relation();
		$data['active'] = 'investors';
		$this->load->view('investor_view', $data);
	}
}