<?php
/**
 * 
 */
class Blog extends CI_Controller
{
	public $page = "Newsevents";
	function __construct()
	{
		parent::__construct();
		$this->load->model("Blog_model");
		$this->load->model('site_model');
		$this->site_meta = $this->site_model->site_meta();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		$data['title_site'] = $site_meta['title_site'];
		$data['description_site'] = $site_meta['description_site'];
		$data['keywords_site'] = $site_meta['keywords_site'];
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['active'] = 'blog';
		$data['data'] = $this->Blog_model->read_all();
		$this->load->view('blog_view', $data);
	}

	public function view($id)
	{
		$site_meta = $this->site_meta;
		$data['data'] = $this->Blog_model->get_by_id($id);
		$data['title_site'] = ucwords($this->Blog_model->get_by_id($id)->title_eventnews) . " - Mofatama Energi";
		$data['description_site'] = ucwords($this->Blog_model->get_by_id($id)->title_eventnews);
		$data['keywords_site'] = $this->Blog_model->get_by_id($id)->title_eventnews;
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['active'] = 'blog';
		$this->load->view('post_blog', $data);
	}
}