<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("AboutModel", 'about');
		$this->load->model('site_model');
		$this->site_meta = $this->site_model->site_meta();
		$this->site_content_home = $this->site_model->site_content_home();
	}

	public function index()
	{
		$site_meta = $this->site_meta;
		$site_content = $this->site_content_home;
		// var_dump($this->site_content_home);
		// echo $this->site_content_home['content_visi'];
		$data = array('title_site'=>$site_meta['title_site']);
		$data['active'] = 'home';
		$data['description_site'] = $site_meta['description_site'];
		$data['keywords_site'] = $site_meta['keywords_site'];
		$data['author_site'] = $site_meta['author_site'];
		$data['theme_color_site'] = $site_meta['theme_color_site'];
		$data['content_about_img'] = $this->site_content_home['content_about_img'];
		$data['content_about_text'] = $this->site_content_home['content_about_text'];
		$data['content_visi'] = $this->site_content_home['content_visi'];
		$data['content_misi'] = $this->site_content_home['content_misi'];
		$data['content_video_url'] = $this->site_content_home['content_video_url'];
		$data['content_video_ratio'] =$this->site_content_home['content_video_ratio'];
		$data['content_video_title'] = $this->site_content_home['content_video_title'];
		$this->load->view('home', $data);
	}
}