<?php
/**
 * 
 */
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username')||$this->session->username!=null){
			redirect(base_url());
		}
		$this->load->model("AdminModel");
	}

	public function index()
	{
		$page = 'login';
		$data = array('page'=>$page);
		$this->load->view('admin/'.$page, $data);
	}

	public function cek_log()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cek = $this->db->query("SELECT * FROM users WHERE username = '$username' AND password = DES_ENCRYPT('$password')");
		$row_num = $cek->num_rows();
		$dt = $cek->row();
		if($row_num>0){
			$this->session->username = $username;
			$this->session->id = $dt->id;
			redirect(base_url('admin'));
		}
	}
}