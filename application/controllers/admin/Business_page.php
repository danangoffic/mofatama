<?php
/**
 * 
 */
class Business_page extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
		$this->load->model("AdminModel");
	}

	public function index()
	{
		$page = "setting_bisnis";
		$databusiness = $this->AdminModel->get_business_data();
		$data = array('page'=>$page, 'databusiness'=>$databusiness);
		$this->load->view('admin/settinbusiness_view', $data);
	}

	public function new_page()
	{
		$page = "setting_bisnis";
		$data = array('nama_business'=>"", 'keterangan_business'=>"", 'page'=>$page, 'cond'=>'save_page');
		$this->load->view('admin/form_business_page', $data);
	}

	public function save_page()
	{
		$nama_business = $this->input->post('nama_business');
		$keterangan_business = $this->input->post('keterangan_business');
		$data = array('nama_business'=> $nama_business, 'keterangan_business'=> $keterangan_business);
		if($this->db->insert('business_connection', $data)):
			redirect("admin/Business_page");
		else:
			redirect('admin/Business_page');
		endif;
	}

	public function edit($id)
	{
		$page = "setting_bisnis";
		$data_query = $this->db->get_where("business_connection", array('id_business'=>$id))->row();
		$data = array('nama_business'=>$data_query->nama_business, 'keterangan_business'=>$data_query->keterangan_business, 'page'=>$page, 'cond'=>'updating_page/'.$data_query->id_business);
		$this->load->view('admin/form_business_page', $data);
	}

	public function updating_page($id)
	{
		$nama_business = $this->input->post('nama_business');
		$keterangan_business = $this->input->post('keterangan_business');
		$data = array('nama_business'=> $nama_business, 'keterangan_business'=> $keterangan_business);
		if($this->db->update('business_connection', $data, array('id_business'=>$id))):
			redirect("admin/Business_page");
		else:
			redirect("admin/Business_page/edit/".$id);
		endif;
	}

	public function delete($id)
	{
		if($this->db->delete('business_connection', array('id_business'=>$id))):
			redirect("admin/Business_page");
		else:
			redirect("admin/Business_page");
		endif;
	}
}