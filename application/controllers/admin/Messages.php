<?php
/**
 * 
 */
class Messages extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
		$this->load->model("AdminModel");
	}

	public function index()
	{
		$page = "messages";
		$datamess = $this->AdminModel->get_all_messages();
		$data = array('page'=>$page, 'datamess'=>$datamess);
		$this->load->view('admin/messages', $data);
	}

	public function view($id)
	{
		$page = "messages";
		$datamess = $this->AdminModel->get_message($id);
		$data = array('page'=>$page, 'datamess'=>$datamess);
		$this->load->view('admin/view_message', $data);
	}

	public function delete($id)
	{
		# code...
	}
}