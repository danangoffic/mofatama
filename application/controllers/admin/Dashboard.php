<?php
/**
 * 
 */
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}

	}

	public function index()
	{
		$page = "dashboard";
		$data = array('page'=>$page);
		$this->load->view('admin/'.$page, $data);
	}
}