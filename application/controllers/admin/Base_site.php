<?php
/**
 * 
 */
class Base_site extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
		$this->load->model("AdminModel");
	}

	public function index()
	{
		$page = "setting_metapage";
		$datameta = $this->AdminModel->get_base_site();
		$data = array('page'=>$page, 'datameta'=>$datameta);
		$this->load->view('admin/settingmeta_view', $data);
	}

	public function save_meta()
	{
		$title_site = $this->input->post('title_site');
		$desc_meta = $this->input->post('desc_meta');
		$keywords_site = $this->input->post('keywords_site');
		$author_site = $this->input->post('author_site');
		$theme_color_site = $this->input->post('theme_color_site');
		$address = $this->input->post('address');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$data = array('title_site'=>$title_site,
					'description_site' => $desc_meta,
					'keywords_site' => $keywords_site,
					'author_site' => $author_site,
					'theme_color_site' => $theme_color_site,
					'address' => $address,
					'phone' => $phone,
					'email' => $email);
		if($this->db->update('site_meta', $data)):
			redirect('admin/Base_site');
		else:
			redirect('admin/Base_site');
		endif;
	}
}