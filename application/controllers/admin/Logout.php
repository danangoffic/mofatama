<?php
/**
 * 
 */
class Logout extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
	}

	public function index()
	{
		$this->session->sess_destroy();
		redirect('admin/Login');
	}
}