<?php
/**
 * 
 */
class Blog_page extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
		$this->load->model('AdminModel');
		date_default_timezone_set("Asia/Jakarta");	//default timezone

	}

	public function index()
	{
		$dataindex = array('page'=>'blogs');
		$this->load->view('admin/blog_view', $dataindex);
	}

	public function get_category()
	{
		header("Content-type:application/json");
		$query = $this->db->get('category_eventnews');
		echo(json_encode($query->result()));
	}

	public function save_category()
	{
		$title = $this->input->post('title_category');
		$insert = array('id_category' => uniqid());
	}

	public function add_post()
	{
		$dataindex = array('page'=>'blogs');
		$this->load->view('admin/addpost_view', $dataindex);
	}

	public function save_post()
	{
		$config['upload_path'] = './images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $title = $this->input->post('title_news');
        $content = $this->input->post('content_news');
        $category = $this->input->post('category');
        $count_cat = count($category);
        $cats = "";
        if($count_cat>1){
            for ($i=0; $i < $count_cat; $i++) {
                if($i+1==$count_cat){
                    $cats =+ ', ';
                }
            }
        }else{
            // echo extract($category);
            // $var = array_chunk($category, 1);
            // echo $var;

            // $cats = $category;
            foreach ($category as $key => $value) {
                $cats = $value;
            }
        }
        $date = date("Y-m-d");
        $time = date("H:i:s");

        $this->upload->initialize($config);

        if(!empty($_FILES['image_news']['name'])){
        	if($this->upload->do_upload('image_news')){
        		$gbr = $this->upload->data();
        		// compress image
        		$config['image_library']='gd2';
                $config['source_image']='./images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 420;
                $config['new_image']= './images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                $data = array('title_eventnews' => $title,
            				'categories_eventnews' => $cats,
            				'content_eventnews' => $content,
            				'image_eventnews' => $gambar,
            				'date_eventnews' => $date,
                            'time_eventnews' => $time,
            				'submitter' => $this->session->id);
                if($this->AdminModel->save_eventnews($data)):
                	redirect('admin/Blog_page');
                endif;
        	}else{
            $data = array('title_eventnews' => $title,
                            'categories_eventnews' => $cats,
                            'content_eventnews' => $content,
                            'date_eventnews' => $date,
                            'time_eventnews' => $time,
                            'submitter' => $this->session->id);
            if($this->AdminModel->save_eventnews_noimage($data)):                
        		redirect('admin/Blog_page/');
            endif;
        	}
        }else{
            $data = array('title_eventnews' => $title,
                            'categories_eventnews' => $cats,
                            'content_eventnews' => $content,
                            'date_eventnews' => $date,
                            'time_eventnews' => $time,
                            'submitter' => $this->session->id);
            if($this->AdminModel->save_eventnews_noimage($data)):                
                redirect('admin/Blog_page/');
            endif;
        }
	}

	public function view_post($id)
	{
		$data = $this->AdminModel->get_eventnews($id);
		$dataindex = array('data'=>$data,'page'=>'blogs');
		$this->load->view('admin/viewpost_view', $dataindex);
	}

	public function saveedit_post($id)
	{
		$title = $this->input->post('title_news');
        $content = $this->input->post('content_news');
        $category = $this->input->post('category');
        $count_cat = count($category);
        $cats = "";
        if($count_cat>1){
        	for ($i=0; $i < $count_cat; $i++) {
        		if($i+1==$count_cat){
        			$cats =+ ', ';
        		}
        	}
        }else{
        	foreach ($category as $key => $value) {
                $cats = $value;
            }
        }
        $date = date("Y-m-d");
        $time = date("H:i:s");
        
		$config['upload_path'] = './images/upload/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);

        if(!empty($_FILES['image_news']['name'])){
        	if($this->upload->do_upload('image_news')){
        		$gbr = $this->upload->data();
        		// compress image
        		$config['image_library']='gd2';
                $config['source_image']='./images/upload/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 420;
                $config['new_image']= './images/upload/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                
                $data = array('title_eventnews' => $title,
            				'categories_eventnews' => $cats,
            				'content_eventnews' => $content,
            				'image_eventnews' => $gambar,
            				'date_eventnews' => $date,
                            'time_eventnews' => $time,
            				'submitter' => $this->session->id);
                if($this->AdminModel->update($data, $id)):
                	redirect('admin/Blog_page');
                endif;
        	}else{
        		$data = array('title_eventnews' => $title,
            				'categories_eventnews' => $cats,
            				'content_eventnews' => $content,
            				'date_eventnews' => $date,
                            'time_eventnews' => $time,
            				'submitter' => $this->session->id);
        	if($this->AdminModel->update_no_image($data,$id)):
        		redirect('admin/Blog_page');
        	endif;
        	}
        }else{
        	$data = array('title_eventnews' => $title,
            				'categories_eventnews' => $cats,
            				'content_eventnews' => $content,
            				'date_eventnews' => $date,
                            'time_eventnews' => $time,
            				'submitter' => $this->session->id);
        	if($this->AdminModel->update_no_image($data,$id)):
        		redirect('admin/Blog_page');
        	endif;
        }
	}

	public function deletepost($id)
	{
		if($this->db->delete('events_news', array('id_eventnews'=>$id))):
			redirect('admin/Blog_page');
		endif;
	}
}