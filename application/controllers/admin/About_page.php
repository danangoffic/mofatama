<?php
/**
 * 
 */
class About_page extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('admin/Login');
		}
		$this->load->model("AdminModel");
	}

	public function index()
	{
		$page = "setting_about";
		$data_about = $this->AdminModel->get_aboutset();
		$data = array('page'=>$page, 'dataabout'=>$data_about);
		$this->load->view('admin/settingabout_view', $data);
	}

	public function add_about_page()
	{
		$this->load->view('admin/add_settingabout_view', array('page'=>'setting_about'));
	}

	public function save_about()
	{
		
	}
}