<?php
/**
 * 
 */
class Blog_model extends CI_Model
{
	public function read_all()
	{
		$query = $this->db->query("SELECT * FROM events_news JOIN category_eventnews ON category_eventnews.id_category = events_news.categories_eventnews JOIN users ON users.id = events_news.submitter");
		return $query->result();
	}

	public function get_by_id($id)
	{
		$where = array('id_eventnews' => $id);
		$query = $this->db->query("SELECT * FROM events_news JOIN category_eventnews ON category_eventnews.id_category = events_news.categories_eventnews JOIN users ON users.id = events_news.submitter WHERE id_eventnews = '$id'");
		return $query->row();
	}
}