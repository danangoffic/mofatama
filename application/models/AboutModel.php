<?php
/**
 * 
 */
class AboutModel extends CI_Model
{
	
	public function get_info_about()
	{
		# code...
	}

	public function all()
	{
		return $this->db->get('about_mofatama');
		
	}
	public function by_kategori($kat)
	{
		$this->db->where('kategori_about', $kat);
		$this->db->select('konten_about');
		return $this->db->get('about_mofatama');
	}
	public function info()
	{
		$this->db->select('address, phone, email');
		return $this->db->get('site_meta');
	}
}