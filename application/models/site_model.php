<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Site_model extends CI_model
{
	
	public function site_meta()
	{
		$query = $this->db->query("SELECT `id`, `title_site`, `description_site`, `keywords_site`, `author_site`, `theme_color_site`, `address`, `phone`, `email` FROM `site_meta`");
		return $query->row_array();
	}

	public function site_content_home()
	{
		$query = $this->db->query("SELECT `content_about_img`, `content_about_text`, `content_visi`, `content_misi`, `content_video_url`, `content_video_ratio`, `content_video_title` FROM `site_content`");
		return $query->row_array();
	}
}