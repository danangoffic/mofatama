<?php
/**
 * 
 */
class Contact_model extends CI_Model
{
	
	public function insert_contact($data)
	{
		$query = $this->db->insert("contactus", $data);
		return $query;
	}

	public function get_info_us()
	{
		$query = $this->db->query("SELECT `address`, `phone`, `email` FROM `site_meta`");
		return $query->row();
	}
}