<?php
/**
 * 
 */
class AdminModel extends CI_Model
{
	
	public function save_eventnews($data)
	{
		$query = $this->db->insert('events_news', $data);
		return $query;
	}

	public function save_eventnews_noimage($data)
	{
		$query = $this->db->insert('events_news', $data);
		return $query;
	}

	public function save_cat_eventnews($data)
	{
		$query = $this->db->insert('category_eventnews', $data);
		return $query;
	}

	public function get_eventnews($id_event)
	{
		$query = $this->db->get_where('events_news', array('id_eventnews'=>$id_event));
		return $query->row();
	}

	public function get_all_eventnews()
	{
		$query = $this->db->query('SELECT events_news.`id_eventnews`, events_news.`title_eventnews`, events_news.`categories_eventnews`, events_news.`content_eventnews`, events_news.`image_eventnews`, events_news.`date_eventnews`, events_news.`submitter`, category_eventnews.title_category, users.username FROM `events_news` JOIN category_eventnews ON category_eventnews.id_category = events_news.categories_eventnews JOIN users ON users.id = events_news.submitter');
		return $query->result();
	}

	public function update($data ,$id_event)
	{
		$this->db->where('id_eventnews', $id_event);
		$query = $this->db->update('events_news', $data);
		return $query;
	}

	public function update_no_image($data, $id_event)
	{
		$this->db->where('id_eventnews', $id_event);
		$query = $this->db->update('events_news', $data);
		return $query;
	}

	public function get_aboutset()
	{
		$query = $this->db->query('SELECT * FROM about_mofatama ORDER BY order_item ASC');
		return $query->result();
	}

	public function get_base_site()
	{
		$query = $this->db->get('site_meta');
		return $query->row();
	}

	public function get_business_data()
	{
		$query = $this->db->get('business_connection');
		return $query->result();
	}

	public function get_all_messages()
	{
		return $this->db->get('contactus')->result();
	}

	public function get_message($id)
	{
		return $this->db->get_where('contactus', array('no = ', $id))->row();
	}
}