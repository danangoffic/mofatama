<?php
/**
 * 
 */
class About_model extends CI_Model
{
	
	public function all()
	{
		$query = $this->db->query('SELECT *, (select max(order_item) from about_mofatama) as last_order, (select min(order_item) from about_mofatama) as first_order FROM about_mofatama ORDER BY order_item ASC');
		return $query->result();
	}

	public function by_id($id)
	{
		$this->db->where('id_about', $id);
		return $this->db->get('about_mofatama');
	}

	public function update($data, $id)
	{
		$this->db->where('id_about', $id);
		return $this->db->update('about_mofatama', $data);
	}
}