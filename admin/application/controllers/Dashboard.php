<?php
/**
 * 
 */
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		date_default_timezone_set("Asia/Jakarta");	//default timezone
	}

	public function index()
	{
		$page = "dashboard";
		$data = array('page'=>$page);
		$this->load->view($page, $data);
	}
}