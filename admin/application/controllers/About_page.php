<?php
/**
 * 
 */
class About_page extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('username') || $this->session->username==null){
			redirect('Login');
		}
		$this->load->model("About_model", 'about');
		date_default_timezone_set("Asia/Jakarta");	//default timezone
	}

	public function index()
	{
		$page = "setting_about";
		$data_about = $this->about->all();
		$data = array('page'=>$page, 'dataabout'=>$data_about);
		$this->load->view('settingabout_view', $data);
	}

	public function add_about_page()
	{
		$this->load->view('add_settingabout_view', array('page'=>'setting_about'));
	}

	public function save_about()
	{
		$title_about = $this->input->post('title_about');
		$content_news = $this->input->post('content_news');
		$id = $this->input->post('id');
		$data = array(
			'title_about' => $title_about,
			'konten_about' => $content_news);
		if($this->about->update($data, $id)){
			$this->session->set_flashdata('success', 'Berhasil Mengubah Data');
			redirect(base_url('About_page'));
		}else{
			$this->session->set_flashdata('error', 'Gagal Mengubah Data');
			redirect(base_url('About_page/edit/'.$id));
		}
	}

	public function edit($id_about)
	{
		$data = array(
				'data' => $this->about->by_id($id_about)->row(),
				'page'=>'setting_about');
		$this->load->view('edit_settingabout_view', $data);
	}

	public function update_about()
	{
		
	}
}