<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            View Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> 
                                    <a href="<?=base_url('')?>">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-book-open"></i> 
                                <a href="<?=base_url('Blog_page')?>">Posts Blog</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-pen"></i> Edit Post
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <form id="form-new-post" action="<?=base_url('Blog_page/saveedit_post/').$data->id_eventnews;?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                            <div class="form-group">
                                
                                <input type="text" class="form-control input-bg" value="<?=ucwords($data->title_eventnews);?>" name="title_news" id="title_news" placeholder="Title" autocomplete="off" autofocus/>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control input-bg" name="content_news" id="content_news" placeholder="Content..">
                                    <?=$data->content_eventnews;?>
                                </textarea>
                            </div>
                            <div class="form-group">
                                <div class="thumbnail">
                                     <img src="<?=base_url('images/upload/').$data->image_eventnews;?>" class="img-rounded" width="100%" >
                                     <div class="caption">
                                         <input class="form-control input-sm" type="file" accept="image/*" name="image_news" id="image_news">
                                     </div>
                                </div>
                                <!-- <div class="col-lg-4">
                                   
                                </div>
                                <div class="col-lg-8">
                                    
                                </div> -->
                            </div>
                            <div class="form-group">
                                <label>Category:</label>
                            </div>
                            <div class="form-group well" id="category_news">
                                <?php
                                $query_cat = $this->db->get('category_eventnews')->result();
                                $nmr = 0;
                                foreach ($query_cat as $row) {
                                    ?>
                                    <div class="checkbox">
                                      <label>
                                        <input <?php if($data->categories_eventnews==$row->id_category){echo "checked";} ?> name="category[<?=$nmr;?>]" id="cats" type="checkbox" value="<?=$row->id_category;?>"><?=$row->title_category;?>
                                        </label>
                                    </div>
                                    <?php
                                    $nmr++;
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-danger btn-sm" type="button" onclick="return window.location.assign('<?=base_url('Blog_page');?>')">Cancel</button>
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <script type="text/javascript">
          $(function () {
            CKEDITOR.replace('content_news');
          });
        </script>
        <!-- END JS SCRIPT -->  
</body>
</html>