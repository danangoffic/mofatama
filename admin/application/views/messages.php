<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-envelope"></i> Messages
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-condensed table-bordered table-hover table-striped" id="tabel_eventsnews">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Sender</th>
                                    <th class="text-center">Subject</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($datamess as $row) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no;?></td>
                                        <td class="text-center">
                                            <a href="mailto:<?=$row->email;?>"><?=$row->email?></a>
                                        </td>
                                        <td class="text-center"><?=$row->fullname;?></td>
                                        <td class="text-center"><?=$row->subject;?></td>
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url('Messages/view/').$row->no;?>">View</a>
                                            <a class="btn btn-danger btn-sm" href="<?=base_url('Messages/delete/').$row->no;?>">Delete Post</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>