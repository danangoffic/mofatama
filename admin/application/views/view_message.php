<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-envelope"></i> Messages
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h2><?=$datamess->subject;?></h2>
                    </div>
                    <div class="col-lg-12">
                        From: <?=$datamess->fullname;?> (<a href="mailto:<?=$datamess->email?>"><?=$datamess->email;?></a>)
                    </div>
                    <div class="col-lg-12">
                        <div class="well">
                            <?=$datamess->message;?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <a class="btn btn-primary" href="<?=base_url('Messages');?>"><i class="fa fa-chevron-left"></i> Back</a>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>