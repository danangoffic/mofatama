<nav class="navbar navbar-default navbar-fixed-top" style="background-color: #000;" role="navigation" style="display: block">    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <p class="navbar-text" style="color: #FFF"><?=ucfirst($this->session->username);?></p>
    </div>
    <!-- /.navbar-header -->
    
    <ul class="nav navbar-right top-nav">
        <li>
            <a href="<?=base_url('admin/Logout')?>" style="color: #FFF"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    

    <?php $this->load->view('templates/sidebar'); ?>
</nav>
