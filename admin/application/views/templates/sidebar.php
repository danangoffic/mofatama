 <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li class="side-user">
            <a class="text-center">
            <!-- <img class="img-rounded" src="<?=base_url('library/kebumen.jpg')?>" alt="" width="100"> -->
            <br>MOFATAMA ENERGI<br>JAKARTA</a>
        </li>
        <li <?=active('dashboard', $page);?>>
            <a href="<?=base_url()?>"><i class="fa fa-fw fa-home"></i> Halaman Utama</a>
        </li>
        <li <?=active('blogs', $page)?>>
            <a href="<?=base_url('Blog_page')?>"><i class="fa fa-fw fa-book-open"></i> Posts Blog</a>
        </li>
        <li <?=active('setting_about', $page)?>>
            <a href="<?=base_url('About_page')?>"><i class="fa fa-fw fa-user"></i> Setting About Site</a>
        </li>
        <li <?=active('setting_metapage', $page)?>>
            <a href="<?=base_url('Base_site')?>"><i class="fa fa-fw fa-tasks"></i> Setting Meta Site</a>
        </li>
        <li <?=active('setting_bisnis', $page)?>>
            <a href="<?=base_url('Business_page')?>"><i class="fa fa-fw fa-th-list"></i> Setting Bisnis Kami</a>
        </li>
        <li <?=active('setting_investor', $page)?>>
            <a href="<?=base_url('setting_investor')?>"><i class="fa fa-fw fa-th-list"></i> Setting Investor</a>
        </li>
        <li <?=active('messages', $page)?>>
            <a href="<?=base_url('messages')?>"><i class="fa fa-fw fa-envelope"></i> Messages</a>
        </li>
        <li <?=active('Users', $page)?>>
            <a href="<?=base_url('Users')?>"><i class="fa fa-fw fa-users-cog"></i> Manage Users</a>
        </li>
        <li <?=active('ubahPassword', $page)?>>
            <a href="<?=base_url('UbahPassword');?>"><i class="fa fa-fw fa-lock"></i> Ubah Password</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->