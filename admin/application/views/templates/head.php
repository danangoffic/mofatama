<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Mofatama Energi Admin</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url('library/css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('library/css/sb-admin.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('library/css/plugins/morris.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('library/font-awesome/css/all.css')?>">
	<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .navbar-default .navbar-nav>.active>a{
            background-color: #FFFFFF !important;
            color: #000000 !important
        }
        ul li a:hover{
            color: #18d26e !important;
        }
    </style>	
</head>