<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-wrench"></i> Setting About
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    <div class="col-lg-8">
                        <a class="btn btn-primary btn-sm pull-right" href="<?=base_url('About_page/')?>add_about_page"><i class="fa fa-fw fa-plus "></i>Add Set</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <?php if($this->session->flashdata('success')):?>
                        <div class="alert alert-success alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success!</strong> <?=$this->session->flashdata('success');?>
                        </div>
                        <?php endif;?>
                        <?php if($this->session->flashdata('error')):?>
                        <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Error!</strong> <?=$this->sesion->flashdata('error');?>
                        </div>
                        <?php endif;?>
                        <?php 
                        if($dataabout!="" || !empty($dataabout) || $dataabout != 0):
                        ?>
                            <table class="table table-condensed table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Title</th>
                                        <th class="text-center">Urutan</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                foreach ($dataabout as $row):
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no;?></td>
                                        <td class="text-center"><?=$row->title_about;?></td>
                                        <td class="text-center"><?=$row->order_item;?></td>
                                        <td class="text-center">
                                            <a class="btn btn-default btn-xs" href="<?=base_url('About_page/edit/').$row->id_about;?>">Edit</a>
                                            <a class="btn btn-danger btn-xs" href="<?=base_url('About_page/delete/').$row->id_about;?>">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                endforeach;
                                ?>
                                </tbody>
                            </table>
                        <?php
                    else:
                    ?>
                    <h2>Data Kosong, Silahkan tambahkan kategori 'Tentang' pada tombol <i class="fa fa-fw fa-plus-circle"></i></h2>
                    <?php
                    endif;
                    ?>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->


    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <!-- END JS SCRIPT -->  
</body>
</html>