<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('templates/head'); ?>
<body>
    <div id="wrapper">
        <!-- NAVIGATION -->
        <?php $this->load->view('templates/nav'); ?>
        <!-- END NAVIGATION -->
        <!-- CONTENT -->
         <div id="page-wrapper" style="min-height: 650px">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <h1 class="page-header">
                            Posts Blog <small>&nbsp;</small>
                        </h1> -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> Dashboard
                            </li>
                            <li>
                                <i class="fa fa-book-open"></i> About
                            </li>
                            <li class="active">
                                <i class="fa fa-plus-circle"></i> Add About Set
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row" style="margin-bottom:10px;">
                    
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <form id="form-new-post" action="<?=base_url('About_page/save_about');?>" method="POST" enctype="multipart/form-data" autocomplete="off">
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?=$data->id_about;?>">
                                <input type="text" class="form-control input-bg" required="" name="title_about" id="title_about" placeholder="Title" autocomplete="off"/ value="<?=$data->title_about?>">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control input-bg" name="content_news" id="content_news" placeholder="Content..">
                                    <?=$data->konten_about;?>
                                </textarea>
                            </div>

                            <!-- <div class="form-group">
                                <input class="form-control input-sm" type="file" accept="image/*" name="image_news" id="image_news">
                            </div> -->
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-default btn-sm" type="button" onclick="return window.location.assign('<?=base_url('About_page');?>')">Cancel</button>
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        
                    </div>

            </div>
            <!-- /.container-fluid -->
            <!-- /.CONTENT -->

        </div>
        <!-- /#page-wrapper -->
        <!-- END CONTENT -->
        
    </div>
        <!-- JS SCRIPT -->
        <?php $this->load->view('templates/script'); ?>
        <script type="text/javascript">
          $(function () {
            CKEDITOR.replace('content_news');
          });
        </script>
        <script type="text/javascript">

            $("#save_category1").click(function(){
                $.post("<?=base_url('Blog_page/save_category')?>", $("#form_add_category").serialize(), function(es){
                    get_category();
                })
            });
        </script>
        <!-- END JS SCRIPT -->  
</body>
</html>