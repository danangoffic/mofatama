<!DOCTYPE html>
<html>
<title>Mofatama Energi</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
* {box-sizing: border-box;}
<style>

.mySlides {display:none}
.w3-tag, .fa {cursor:pointer}
.w3-tag {height:15px;width:15px;padding:0;margin-top:6px}

body { 
  margin: 0;
  
}

.header {
  overflow: hidden;
  background-color: #f1f1f1;
  padding: 20px 10px;
}

.header a {
  float: left;
  color: black;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 18px; 
  line-height: 25px;
  border-radius: 4px;
}

.header a.logo {
  font-size: 25px;
  font-weight: bold;
}

.header a:hover {
  background-color: #9ACD32;
  color: black;
}

.header a.active {
  color: black;
}

.header-right {
  float: right;
}

@media screen and (max-width: 500px) {
  .header a {
    float: none;
    display: block;
    text-align: left;
  }
  .header-right {
    float: none;
  }
}
.navbar {
    
      margin-bottom: 0;
      
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      
  }
  .navbar li a, .navbar .navbar-brand {
     
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #FFF !important;
      background-color: #3C7 !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  .navbar-brand{
    padding: unset;
  }
</style>
<body>

<!-- Links (sit on top) -->
<!-- <div class="w3-bar w3-border w3-light-green"> 
	<a href="#" class="w3-bar-item w3-button w3-border-right">Home</a>
    <a href="#plans" class="w3-bar-item w3-button w3-border-right">Our Business</a> 
    <a href="#news" class="w3-bar-item w3-button w3-border-right">News & Events</a>
	<a href="#investor" class="w3-bar-item w3-button w3-border-right">Investor Relations</a>
	<a href="#about" class="w3-bar-item w3-button w3-border-right">About</a>
	<a href="#contact" class="w3-bar-item w3-button w3-border-right">Contact</a>
<br></div></br>
-->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php"><img src="/images/newlogo.png" class="media-object" style="width:130px"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a  href="#home">Home</a></li>
        <li><a href="#plans">Our Business</a></li>
        <li><a href="#news">News & Events</a></li>
        <li><a href="#investor">Investor Relations</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </div>
  </div>
</nav>
  

<!-- Content -->

  <!-- Slideshow -->
  <div class="row">
    <div class="w3-container">
      <div class="w3-display-container mySlides w3-animate-fading">
        <img src="/images/mof_4.jpg" style="width:100%; background-size: contain;">
        <div class="w3-display-topleft w3-container w3-padding-32">
        </div>
      </div>
      <div class="w3-display-container mySlides w3-animate-fading">
        <img src="/images/mof_3.jpg" style="width:100%; background-size: contain;">
        <div class="w3-display-middle w3-container w3-padding-32">
        </div>
      </div>
      <div class="w3-display-container mySlides w3-animate-fading">
        <img src="/images/mof_2.jpg" style="width:100%; background-size: contain;">
        <div class="w3-display-topright w3-container w3-padding-32">
        </div>
      </div>
      <div class="w3-display-container mySlides w3-animate-fading">
        <img src="/images/mof_5.jpg" style="width:100%; background-size: contain;">
        <div class="w3-display-topright w3-container w3-padding-32">
        </div>
      </div>
      <div class="w3-display-container mySlides w3-animate-fading">
        <img src="/images/mof_6.jpg" style="width:100%; background-size: contain;">
        <div class="w3-display-topright w3-container w3-padding-32">
        </div>
      </div>
      <!-- Slideshow next/previous buttons -->
      <div class="w3-container w3-light-green w3-padding w3-large">
        <div class="w3-left" onclick="plusDivs(-1)"><i class="fa fa-arrow-circle-left w3-hover-text-teal"></i></div>
        <div class="w3-right" onclick="plusDivs(1)"><i class="fa fa-arrow-circle-right w3-hover-text-teal"></i></div>
      
        <div class="w3-center">
          <span class="w3-tag demodots w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
          <span class="w3-tag demodots w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
          <span class="w3-tag demodots w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
  		    <span class="w3-tag demodots w3-border w3-transparent w3-hover-white" onclick="currentDiv(4)"></span>
  		    <span class="w3-tag demodots w3-border w3-transparent w3-hover-white" onclick="currentDiv(5)"></span>
        </div>
      </div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
      <div class="container-fluid alert alert-success w3-flat-emerald"></br>
        <h1>Mofatama Energi</h1>
        <p class="text-justify">Usaha batubara dimulai pada tahun 2008. Dengan memulai dari bidang trading batubara, baik lokal maupun eksport. 
        Melihat pertumbuhan usaha yang meningkat dari perkembangan batubara yang sangat bagus serta prospek pemasaran yang sangat besar, 
        perusahaan mulai melakukan expansi usaha, yaitu pada tahun 2010 perusahaan mengakuisisi tambang batubara yang berlokasi di Sungai Danau, 
        Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan seluas 41,5 Ha, sehingga sumber batubara selalu terjaga dengan baik untuk memenuhi 
        permintaan pasar. Kemudian berturut-turut di tahun 2011 mengakuisisi koneksi tambang di Barito Utara Provinsi Kalimantan Tengah seluas 193,5 Ha, 
        21.094 Ha, 3211 Ha. dan di Kutai Timur, Provinsi Kalimantan TImur seluas 10.220 Ha. Semua itu dilakukan untuk menambah jumlah cadangan batubara untuk beberapa tahun kedepan, 
        sehingga perusahaan bisa memenuhi permintaan pasar. Saat ini produksi berjalan 250.000 ton perbulan (3 juta pertahun) dan akan terus di tingkatkan kapasitas 
        penambangannya sesuai permintaan.</p>
        <br>
      </div>
    </div>
    
  </div>


  
  <!-- Grid -->
  <div class="w3-row w3-container">
    <div class="w3-center w3-padding-64">
      <span class="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">What We Offer</span>
    </div>
  <div class="w3-col l3 m6 w3-light-green w3-container w3-padding-16">
		<h2>Visi</h2>
		<p>Menjadi perusahaan energi yang handal dan mampu memenuhi komitmen kepada pihak stakeholder.</p>
    </div>
	
	<div class="w3-col l3 m6 w3-green w3-container w3-padding-16">
		<h2>Misi</h2>
		<p>Peningkatan mutu sumber daya manusia yang secara terus menerus, sehingga 
		dapat bekerja secara profesional dan mempunyai kemampuan yang memadai dalam proses pertambangan.</p>
    </div>
  </div>
  <div class="w3-row w3-container">
    <div class="w3-center w3-padding-64">
    </div>
   	
	<div class="w3-col l3 m6 w3-green w3-container w3-padding-16">
      <h3>Design</h3>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
    </div>

    <div class="w3-col l3 m6 w3-light-green w3-container w3-padding-16">
      <h3>Branding</h3>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
    </div>

    <div class="w3-col l3 m6 w3-green w3-container w3-padding-16">
      <h3>Consultation</h3>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
    </div>

    <div class="w3-col l3 m6 w3-light-green w3-container w3-padding-16">
      <h3>Promises</h3>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
    </div>
  </div>

  <!-- Grid -->
  <div class="w3-row-padding" id="plans">
	<div class="w3-center w3-padding-64">
      <h3>Pricing Plans</h3>
      <p>Choose a pricing plan that fits your needs.</p>
    </div>

    <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-center w3-hover-shadow">
        <li class="w3-green w3-xlarge w3-padding-32">Basic</li>
        <li class="w3-padding-16"><b>10GB</b> Storage</li>
        <li class="w3-padding-16"><b>10</b> Emails</li>
        <li class="w3-padding-16"><b>10</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 10</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-green w3-padding-24">
          <button class="w3-button w3-green w3-padding-large">Sign Up</button>
        </li>
      </ul>
    </div>

    <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-center w3-hover-shadow">
        <li class="w3-green w3-xlarge w3-padding-32">Pro</li>
        <li class="w3-padding-16"><b>25GB</b> Storage</li>
        <li class="w3-padding-16"><b>25</b> Emails</li>
        <li class="w3-padding-16"><b>25</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 25</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-green w3-padding-24">
          <button class="w3-button w3-green w3-padding-large">Sign Up</button>
        </li>
      </ul>
    </div>

    <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-center w3-hover-shadow">
        <li class="w3-green w3-xlarge w3-padding-32">Premium</li>
        <li class="w3-padding-16"><b>50GB</b> Storage</li>
        <li class="w3-padding-16"><b>50</b> Emails</li>
        <li class="w3-padding-16"><b>50</b> Domains</li>
        <li class="w3-padding-16"><b>Endless</b> Support</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide">$ 50</h2>
          <span class="w3-opacity">per month</span>
        </li>
        <li class="w3-light-green w3-padding-24">
          <button class="w3-button w3-green w3-padding-large">Sign Up</button>
        </li>
      </ul>
    </div>
  </div>

   <!-- News & Events -->
  <div class="w3-row-padding" id="news">
    <div class="w3-center w3-padding-64">
      <span class="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">News & Events</span>
    </div>
    <div class="w3-third w3-margin-bottom">
      <div class="w3-card-4">
        <div class="w3-container">
          <h3>PT Mofatama Tbk Tawarkan saham IPO</h3>
          <p class="w3-opacity">Media Clipping</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p><button class="w3-button w3-light-green w3-block">Read More</button></p>
        </div>
      </div>
    </div>

    <div class="w3-third w3-margin-bottom">
      <div class="w3-card-4">
        <div class="w3-container">
          <h3>Panggilan Rapat Umum Pemegang Saham Luar Biasa</h3>
          <p class="w3-opacity">Daily News</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p><button class="w3-button w3-light-green w3-block">Read More</button></p>
        </div>
      </div>
    </div>

    <div class="w3-third w3-margin-bottom">
      <div class="w3-card-4">
        <div class="w3-container">
          <h3>PT Mofatama Tbk siap melantai di BEI</h3>
          <p class="w3-opacity">Media Clipping</p>
          <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
          <p><button class="w3-button w3-light-green w3-block">Read More</button></p>
        </div>
      </div>
    </div>
  </div>
 
 
 <!-- About Us -->
  <div class="w3-row-padding" id="about">
    <div class="w3-center w3-padding-64">
      <span class="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">About Us</span>
    </div>
 
  <div class="row">
    <div class="well-lg">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
        <img src="/images/padonny.jpg" class="img-circle" style="width:80px">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <h4 class="heading"><b>R. Donny M. Iskandar</b></h4>
  	  <p>President Director</p>
        <p>Donny memulai karier di Bank Niaga sebagai karyawan di Sudries Department pada tahun 1987. Setelah 6 tahun bekerja, Donny kemudian bergabung dengan PT. Bank Muamalat Indonesia. Di Bank Muamalat kariernya dimulai sebagai Operation Manager di Cabang Fatmawati, Jakarta Selatan, kemudian mengemban jabatan yang cukup variatif sebagai Regional Loan Administration & Legal Department Head, Domestic & International Department Head, Operation Policy & Procedure Department Head, Area Manager dengan jabatan terakhir sebagai Senior Vice President Corporate Banking sebelum kemudian pension pada tahun 2018.</p>
        <p>Pria yang lahir pada 17 Mai 1963 di Jakarta ini memiliki karir yang panjang sebagai banker, menjadikan Donny memiliki keahlian yang cukup baik dalam operational shariah banking, dalam melakukan analisa bisnis, Marketing dan Penjualan serta kompentensi dalam Management Risiko. Selain itu Donny juga memiliki sertifikasi sebagai assessor kompetensi untuk para banker syariah yang diperoleh yang bersangkutan dari Badan Nasional Sertifikasi Profesi (BNSP).</p>
        <p>Lulus dari Magister Economic pada jurusan Islamic Economic & Finance di Universitas Trisakti pada tahun 2018, Donny saat ini sedang melanjutkan pendididikan doctoral di kampus yang sama dengan jurusan Islamic Finance.</p>
      </div>
    </div>
  </div>
  <hr>
  
  <div class="row">
    <div class="well-lg">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
        <img src="/images/burini.jpg" class="img-circle" style="width:80px;">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <h4 class="heading"><b>Rini Kadarwati</b></h4>
        <p>Finance Director</p>
        <p>Lahir di Bandung pada 2 Desember 1971, Rini menyelesaikan pendidikan strata 1 di Institut Teknologi Bandung (ITB) Jurusan Teknologi Lingkungan pada tahun 1989. Karirnya dimulai pada tahun 1995 diperusahaan konsultan di Cilaki, Bandung. Pada tahun 1997 Rini bergabung di PT. Bank Muamalat Indonesia, Tbk  sebagai Management Trainee. Berkarir selama 20 tahun di dunia perbankan, Rini memiliki pengalaman yang cukup panjang dimulai dari Account Manager, menjadi Branch Manager, Area Manager hingga menjadi Kepala Divisi Corporate Banking.</p>
        <p>Lama di bank, Rini memiliki kompetensi yang baik dalam operational banking terutama pada bank syariah, melakukan analisa pembiayaan dan keuangan, serta memiliki pemahaman atas pengembangan dan strategi bisnis</p>
        <p>Setelah memiliki karir yang panjang di Muamalat yaitu selama 17 tahun; Rini kemudian berkarir di PT. Bank Panin Syariah  dengan jabatan terakhir sebagai Commercial Financing Group Head sebelum kemudian pada pertengahan tahun 2018 menjabat sebagai CFO di PT. Mofatama Energi.</p>
      </div>
    </div>
  </div>
  <hr>
  
  
</div>

<div class="w3-center w3-padding-64">
      <span class="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">Organization Structural</span>
<img src="/images/strukturbocbod.png" alt="" class="center" style="width:900px">
</div></body>

  <!-- Contact -->
  <div class="w3-center w3-padding-64" id="contact">
    <span class="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">Contact Us</span>
  </div>

  <form action="/action_page.php" class="w3-container w3-card-4 w3-light-grey w3-text-green w3-margin">
<h2 class="w3-center"></h2>
 
<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="first" type="text" placeholder="First Name">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="last" type="text" placeholder="Last Name">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="email" type="text" placeholder="Email">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="phone" type="text" placeholder="Phone">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-pencil"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="message" type="text" placeholder="Message">
    </div>
</div>

<button class="w3-button w3-block w3-section w3-light-green w3-ripple w3-padding">Send</button>

</form>

</body>

<!-- Footer -->

<footer class="w3-container w3-padding-32 w3-light-green w3-center">
  
  <h4>Head Office</h4>
  <a href="#" class="w3-button w3-black w3-margin"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
  <div class="w3-xlarge w3-section">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
	
<div class="row">
  <div class="column" style="background-color:#aaa;">
    <h2>PT Mofatama Energi Tbk</h2>
    <p>Gedung Graha CIMB Niaga 
    Lantai 11, Jl. Jend Sudirman Kav. 58</p>
    <p>Telp. (021) 52971778</p>
  </div>
  </div>
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="Mofatama Energi" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>


<script>
// Slideshow
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demodots");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
}
</script>

</body>
</html>