-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2018 at 02:59 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mofatama`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_mofatama`
--

CREATE TABLE IF NOT EXISTS `about_mofatama` (
  `id_about` int(11) NOT NULL,
  `kategori_about` varchar(35) NOT NULL,
  `title_about` varchar(40) NOT NULL,
  `konten_about` text NOT NULL,
  `order_item` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `business_connection`
--

CREATE TABLE IF NOT EXISTS `business_connection` (
  `id_business` int(5) NOT NULL,
  `nama_business` varchar(40) NOT NULL,
  `keterangan_business` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_connection`
--

INSERT INTO `business_connection` (`id_business`, `nama_business`, `keterangan_business`) VALUES
(1, 'PT Rizki Utama Indobara', ''),
(2, 'PT Indo Besi Energi Utama', NULL),
(3, 'PT Tidung Bangun Persada', NULL),
(4, 'PT Arta Usaha Bahagia', NULL),
(5, 'PT Arta Usaha Sarana', NULL),
(6, 'PT Persada Raya Energi', NULL),
(8, 'PT Unggul Bangun Persada', '');

-- --------------------------------------------------------

--
-- Table structure for table `category_eventnews`
--

CREATE TABLE IF NOT EXISTS `category_eventnews` (
  `id_category` varchar(10) NOT NULL,
  `title_category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_eventnews`
--

INSERT INTO `category_eventnews` (`id_category`, `title_category`) VALUES
('ev', 'event'),
('news', 'news');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `no` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(30) DEFAULT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`no`, `fullname`, `email`, `subject`, `message`) VALUES
(1, 'danang', 'dananag@mail.com', 'subject 123', 'message');

-- --------------------------------------------------------

--
-- Table structure for table `events_news`
--

CREATE TABLE IF NOT EXISTS `events_news` (
  `id_eventnews` int(11) NOT NULL,
  `title_eventnews` varchar(150) NOT NULL,
  `categories_eventnews` varchar(10) NOT NULL,
  `content_eventnews` text NOT NULL,
  `image_eventnews` varchar(40) DEFAULT NULL,
  `date_eventnews` date NOT NULL,
  `time_eventnews` time NOT NULL,
  `submitter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_news`
--

INSERT INTO `events_news` (`id_eventnews`, `title_eventnews`, `categories_eventnews`, `content_eventnews`, `image_eventnews`, `date_eventnews`, `time_eventnews`, `submitter`) VALUES
(9, 'event 1', 'ev', '<p>event 1 2 3 blablabla</p>\r\n', NULL, '2018-09-28', '16:43:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `header_slider`
--

CREATE TABLE IF NOT EXISTS `header_slider` (
  `id_slide` varchar(10) NOT NULL,
  `img_src` varchar(30) NOT NULL,
  `urutan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `header_slider_direction`
--

CREATE TABLE IF NOT EXISTS `header_slider_direction` (
  `id_direction` varchar(20) NOT NULL,
  `id_slide` varchar(10) NOT NULL,
  `layer1_title` varchar(30) NOT NULL,
  `layer2_title` varchar(30) NOT NULL,
  `layer3_btn_title_a` varchar(10) NOT NULL,
  `layer3_btn_title_b` varchar(10) NOT NULL,
  `layer3_btn_url_a` varchar(20) NOT NULL,
  `layer3_btn_url_b` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor_financial`
--

CREATE TABLE IF NOT EXISTS `investor_financial` (
  `id_investor` int(5) NOT NULL,
  `kategori_investor` varchar(25) DEFAULT NULL,
  `isi_investor` text,
  `file_url` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_content`
--

CREATE TABLE IF NOT EXISTS `site_content` (
  `id_content` int(3) NOT NULL,
  `content_about_img` varchar(40) DEFAULT NULL,
  `content_about_text` text,
  `content_visi` text,
  `content_misi` text,
  `content_video_url` varchar(40) DEFAULT NULL,
  `content_video_ratio` int(5) DEFAULT NULL,
  `content_video_title` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_content`
--

INSERT INTO `site_content` (`id_content`, `content_about_img`, `content_about_text`, `content_visi`, `content_misi`, `content_video_url`, `content_video_ratio`, `content_video_title`) VALUES
(1, '', 'Usaha batubara dimulai pada tahun 2008. Dengan memulai dari bidang trading batubara, baik lokal maupun eksport. Melihat pertumbuhan usaha yang meningkat dari perkembangan batubara yang sangat bagus serta prospek pemasaran yang sangat besar, perusahaan mulai melakukan expansi usaha, yaitu pada tahun 2010 perusahaan mengakuisisi tambang batubara yang berlokasi di Sungai Danau, Kabupaten Tanah Bumbu, Provinsi Kalimantan Selatan seluas 41,5 Ha, sehingga sumber batubara selalu terjaga dengan baik untuk memenuhi permintaan pasar. Kemudian berturut-turut di tahun 2011 mengakuisisi koneksi tambang di Barito Utara Provinsi Kalimantan Tengah seluas 193,5 Ha, 21.094 Ha, 3211 Ha. dan di Kutai Timur, Provinsi Kalimantan TImur seluas 10.220 Ha. Semua itu dilakukan untuk menambah jumlah cadangan batubara untuk beberapa tahun kedepan, sehingga perusahaan bisa memenuhi permintaan pasar. Saat ini produksi berjalan 250.000 ton perbulan (3 juta pertahun) dan akan terus di tingkatkan kapasitas penambangannya sesuai permintaan.', 'Menjadi perusahaan energi yang handal dan mampu memenuhi komitmen kepada pihak stakeholder.', 'Peningkatan mutu sumber daya manusia yang secara terus menerus, sehingga dapat bekerja secara profesional dan mempunyai kemampuan yang memadai dalam proses pertambangan.', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_meta`
--

CREATE TABLE IF NOT EXISTS `site_meta` (
  `id` int(11) NOT NULL,
  `title_site` varchar(20) DEFAULT NULL,
  `description_site` varchar(200) DEFAULT NULL,
  `keywords_site` varchar(30) DEFAULT NULL,
  `author_site` varchar(50) DEFAULT NULL,
  `theme_color_site` varchar(7) DEFAULT NULL,
  `address` text,
  `phone` char(13) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_meta`
--

INSERT INTO `site_meta` (`id`, `title_site`, `description_site`, `keywords_site`, `author_site`, `theme_color_site`, `address`, `phone`, `email`) VALUES
(1, 'Mofatama Energ', '', 'Pertambangan, Mofatama, Energi', '', '33CC77', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `rank_user` varchar(25) DEFAULT NULL,
  `password` varchar(90) NOT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `rank_user`, `password`, `last_login`) VALUES
(1, 'admin', NULL, '€9$ó7Rö}', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_mofatama`
--
ALTER TABLE `about_mofatama`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `business_connection`
--
ALTER TABLE `business_connection`
  ADD PRIMARY KEY (`id_business`);

--
-- Indexes for table `category_eventnews`
--
ALTER TABLE `category_eventnews`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`no`), ADD FULLTEXT KEY `message` (`message`);

--
-- Indexes for table `events_news`
--
ALTER TABLE `events_news`
  ADD PRIMARY KEY (`id_eventnews`), ADD KEY `categories_eventnews` (`categories_eventnews`), ADD KEY `submitter` (`submitter`);

--
-- Indexes for table `header_slider`
--
ALTER TABLE `header_slider`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `header_slider_direction`
--
ALTER TABLE `header_slider_direction`
  ADD KEY `id_slide` (`id_slide`);

--
-- Indexes for table `investor_financial`
--
ALTER TABLE `investor_financial`
  ADD PRIMARY KEY (`id_investor`);

--
-- Indexes for table `site_content`
--
ALTER TABLE `site_content`
  ADD PRIMARY KEY (`id_content`);

--
-- Indexes for table `site_meta`
--
ALTER TABLE `site_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_mofatama`
--
ALTER TABLE `about_mofatama`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `business_connection`
--
ALTER TABLE `business_connection`
  MODIFY `id_business` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events_news`
--
ALTER TABLE `events_news`
  MODIFY `id_eventnews` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `investor_financial`
--
ALTER TABLE `investor_financial`
  MODIFY `id_investor` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_content`
--
ALTER TABLE `site_content`
  MODIFY `id_content` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_meta`
--
ALTER TABLE `site_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `events_news`
--
ALTER TABLE `events_news`
ADD CONSTRAINT `events_news_ibfk_2` FOREIGN KEY (`submitter`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `header_slider_direction`
--
ALTER TABLE `header_slider_direction`
ADD CONSTRAINT `header_slider_direction_ibfk_1` FOREIGN KEY (`id_slide`) REFERENCES `header_slider` (`id_slide`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
